
// Playing Cards
// Ethan Corrigan

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum class Suit
{
	Diamonds,
	Hearts,
	Spades,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};



int main()
{
	Card c1;
	c1.rank = Rank::Ten;

	Card c2;
	c2.rank = Rank::Ace;



	(void)_getch();
	return 0;
}
